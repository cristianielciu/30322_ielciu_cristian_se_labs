package Lab9.Ex3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class ReadTextFile extends JFrame {
    private TextField textField;
    private JButton button;
    private TextArea textArea;

    ReadTextFile(){
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(450,600);
        init();

        setVisible(true);

    }

    private void init(){
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();

        panel.setBounds(0,0 ,400, 600);

        button = new JButton("Read file");
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        panel.add(button,c);

        button.addActionListener(new ButtonPress() );

        textField = new TextField();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.weightx = 0.5;
        c.gridx = 1;
        c.gridy = 0;
        panel.add(textField,c);

        textArea = new TextArea();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.fill = GridBagConstraints.VERTICAL;
        c.weighty = 0.5;
        c.gridx = 0;
        c.gridy = 1;
        c.gridwidth = 2;
        panel.add(textArea,c);

        add(panel);
    }

    public static void main(String[] args) {
        new ReadTextFile();
    }

    class ButtonPress implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            try
            {   textArea.setText("");
                BufferedReader bf =
                        new BufferedReader(
                                new FileReader(textField.getText()));
                String l = "";
                l = bf.readLine();

                while (l!=null)
                {
                    textArea.append(l+"\n");
                    l = bf.readLine();
                }
            }catch(Exception ex){}
        }
    }
}
