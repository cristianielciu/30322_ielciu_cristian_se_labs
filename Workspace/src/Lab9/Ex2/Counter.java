package Lab9.Ex2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Counter extends JFrame {
    int counter = 0;
    private JButton button;
    private TextField textField;

    public Counter(){
        setTitle("Counter");
        setSize(300,90);

        setDefaultCloseOperation(EXIT_ON_CLOSE);

        init();

        setVisible(true);
    }

    private void init(){
        int width = 150;
        int height = 20;
        setLayout(null);

        button = new JButton("Count on me");
        button.setBounds(40,20 , width, height);

        button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                counter++;
                textField.setText("" + counter);
            }
        });

        textField = new TextField();
        textField.setBounds(200, 20, (int) (0.3*width), height);
        textField.setEditable(false);

        add(button);
        add(textField);
    }

    public static void main(String[] args) {
        new Counter();
    }
}
