package Lab9.Ex4;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class TicTacToe extends JFrame {
    JButton b1, b2, b3, b4, b5, b6, b7, b8, b9;
    JButton restartButton;
    JPanel board;
    private boolean x;

    TicTacToe(){
        x = true;

        setTitle("TicTacToe");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setSize(300,400);
        init();

        setVisible(true);
    }

    private void init(){
        initButtons();
        restartButton = new JButton("Restart Game") ;
        board = new JPanel();
        board.setLayout(new GridBagLayout());
        restartButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                b1.setText(""); b2.setText("");b3.setText(""); b4.setText(""); b5.setText("");
                b6 .setText(""); b7.setText(""); b8.setText(""); b9.setText("");
            }
        });

        GridBagConstraints c = new GridBagConstraints();

        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = 0.1;
        c.weighty = 0.1;
        board.add(b1,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridx = 1;
        c.gridy = 0;
        c.weightx = 0.1;
        c.weighty = 0.1;
        board.add(b2,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridx = 2;
        c.gridy = 0;
        c.weightx = 0.1;
        c.weighty = 0.1;
        board.add(b3,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        board.add(b4,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridx = 1;
        c.gridy = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        board.add(b5,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridx = 2;
        c.gridy = 1;
        c.weightx = 0.1;
        c.weighty = 0.1;
        board.add(b6,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridx = 0;
        c.gridy = 2;
        c.weightx = 0.1;
        c.weighty = 0.1;
        board.add(b7,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridx = 1;
        c.gridy = 2;
        c.weightx = 0.1;
        c.weighty = 0.1;
        board.add(b8,c);

        c.fill = GridBagConstraints.BOTH;
        c.gridx = 2;
        c.gridy = 2;
        c.weightx = 0.1;
        c.weighty = 0.1;
        board.add(b9,c);

        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 3;
        c.gridwidth = 3;
        c.weightx = 0.1;
        c.weighty = 0.15;
        board.add(restartButton,c);


        board.setVisible(true);
        add(board);
    }

    public static void main(String[] args) {
        new TicTacToe();
    }

    private void initButtons(){
        b1 = new JButton();
        b1.setBackground(Color.white);
        b1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(x){
                    b1.setText("X");
                }
                else b1.setText("O");

                x = !x;
            }
        });

        b2 = new JButton();
        b2.setBackground(Color.white);
        b2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(x){
                    b2.setText("X");
                }
                else b2.setText("O");
                x = !x;
            }
        });

        b3 = new JButton();
        b3.setBackground(Color.white);
        b3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(x){
                    b3.setText("X");
                }
                else b3.setText("O");
                x = !x;
            }
        });

        b4 = new JButton();
        b4.setBackground(Color.white);
        b4.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(x){
                    b4.setText("X");
                }
                else b4.setText("O");
                x = !x;
            }
        });

        b5 = new JButton();
        b5.setBackground(Color.white);
        b5.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(x){
                    b5.setText("X");
                }
                else b5.setText("O");
                x = !x;
            }
        });

        b6 = new JButton();
        b6.setBackground(Color.white);
        b6.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(x){
                    b6.setText("X");
                }
                else b6.setText("O");
                x = !x;
            }
        });

        b7 = new JButton();
        b7.setBackground(Color.white);
        b7.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(x){
                    b7.setText("X");
                }
                else b7.setText("O");
                x = !x;
            }
        });

        b8 = new JButton();
        b8.setBackground(Color.white);
        b8.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(x){
                    b8.setText("X");
                }
                else b8.setText("O");
                x = !x;
            }
        });

        b9 = new JButton();
        b9.setBackground(Color.white);
        b9.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(x){
                    b9.setText("X");
                }
                else b9.setText("O");
                x = !x;
            }
        });
    }

}
