package Lab3;

public class TestRobot {
    public static void main(String[] args){
        Robot robot = new Robot();

        System.out.println(robot.toString());

        robot.change(10);

        System.out.println(robot); //printing an object will call the toString automatically
    }
}
