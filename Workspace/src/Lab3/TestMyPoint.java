package Lab3;

public class TestMyPoint {
    public static void main(String[] args){
        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint(2,2);

        System.out.println("p1: " + p1.toString());
        p1.setX(1);
        p1.setY(3);
        System.out.println(("p1: " + p1.toString()));
        System.out.println(("p2: " + p2.toString()));

        System.out.println("getX example(p1): " + p1.getX());
        System.out.println("getY example(p1): " + p1.getY());

        System.out.println("distance between p1 and p2: " + p1.distance(p2));
        System.out.println("distance between p2 and (4,4): " + p2.distance(4,4));
    }
}
