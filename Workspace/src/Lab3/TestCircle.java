package Lab3;

public class TestCircle {
    public static void main(String[] args){
        Circle c = new Circle();
        Circle c1 = new Circle(2, "green");

        System.out.println("The radius of the first circle is " + c.getRadius());
        System.out.println("Area of the first circle is " + c.getArea());
        System.out.println("The radius of the second circle is " + c1.getRadius());
        System.out.println("Area of the second circle is " + c1.getArea());
    }
}
