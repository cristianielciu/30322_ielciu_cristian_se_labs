package Lab3;

public class TestAuthor {
    public static void main(String[] args){
        Author author = new Author("Mihai Popa", "mihai.popa@gmail.com", 'm');

        System.out.println(author.toString());

        author.setEmail("mihai.popa@yahoo.com");
        System.out.println("name: " + author.getName());
        System.out.println("email: " + author.getEmail());
        System.out.println("gender: " + author.getGender());
    }
}
