package Lab4.ex6;

public class Square extends Rectangle{
    public Square(){
        super(); //to initialize all the attributes
    }
    public Square(double side){
        super.setWidth(side);// or you can call the super constructor of the rectangle
    }
    public Square(double side, String color, boolean filled){
        super.setWidth(side); //same as the previous note
        super.setColor(color);
        super.setFilled(filled);
    }
    public double getSide(){
        return getWidth();
    }
    public void setSide(double side){
        setWidth(side);
    }

    @Override
    public void setLength(double length){
        super.setWidth(length);
    }

    @Override
    public void setWidth(double width){
        super.setWidth(width);
    }
    @Override
    public double getArea(){ return Math.pow(getWidth(),2);}

    @Override
    public double getPerimeter(){ return 4*getWidth();}

    @Override
    public String toString(){
        return "A Square with side = " + getSide() + ", which is a subclass of " + super.toString();
    }
}
