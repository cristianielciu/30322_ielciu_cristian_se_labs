package Lab4.ex5;


public class TestCylinder {
    public static void main(String[] args){
        Cylinder cylinder1 =  new Cylinder(2);
        Cylinder cylinder2 = new Cylinder(2, 10);

        System.out.println("A1 = " + cylinder1.getArea());
        System.out.println("h1 = " + cylinder1.getHeight());
        System.out.println("V1 = " + cylinder1.getVolume());
        System.out.println();
        System.out.println("A2 = " + cylinder2.getArea());
        System.out.println("h2 = " + cylinder2.getHeight());
        System.out.println("V2 = " + cylinder2.getVolume());
    }
}
