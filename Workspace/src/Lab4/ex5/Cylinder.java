package Lab4.ex5;

import Lab4.ex1.Circle;

public class Cylinder extends Circle {
    private double height;

    public Cylinder(){
        super();// to always initialize all the attributes
        height = 1.0;
    }
    public Cylinder(double radius){
        super(radius);
        height = 1.0;
    }
    public Cylinder(double radius, double height){
        super(radius);
        this.height = height;
    }

    public double getHeight(){
        return height;
    }
    public double getVolume(){
        return super.getArea() * height;
    }

    @Override
    public double getArea(){
        return 2 * Math.PI * Math.pow(getRadius(),2) + 2 * Math.PI * getRadius() * height;
    }
}
