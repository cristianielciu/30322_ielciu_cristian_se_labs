package Lab4.ex3;
//ex4 too

import Lab4.ex2.Author;

public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock;

    public Book(String name, Author[] authors, double price){
        this.name = name;
        this.authors = authors;
        this.price = price;
        qtyInStock = 0;
    }
    public Book(String name, Author[] authors, double price, int qtyInStock){
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName(){
        return name;
    }


    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice(){
        return price;
    }

    public void setPrice(double price){
        this.price = price;
    }
    public int getQtyInStock(){
        return qtyInStock;
    }
    public void setQtyInStock(int qtyInStock){
        this.qtyInStock = qtyInStock;
    }

    public void printAuthors(){
        for (Author aut: authors) {
            System.out.println(aut.getName());
        }
    }

    // Printing all authors
//    @Override
//    public String toString(){
//        String result = "'" + name + "' by ";
//        for(int i = 0; i < authors.length-1; i++){
//            result += authors[i] + ", ";
//        }
//        result += authors[authors.length-1];
//        return result;
//    }
    @Override
    public String toString()
    {
        return name + " by " + authors.length + " authors";
    }

}
