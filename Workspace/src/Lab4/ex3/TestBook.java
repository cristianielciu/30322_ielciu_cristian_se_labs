package Lab4.ex3;


import Lab4.ex2.Author;

public class TestBook {
    public static void main(String[] args){
        //testing constructors
        Book book1 = new Book("Harry Potter and the Philosopher's Stone", new Author[]{new Author("J. K. Rowling", "jkrowling@gmail.com", 'f')}, 50);
        Book book2 = new Book("Harry Potter and the Chamber of Secrets", new Author[]{new Author("J. K. Rowling", "jkrowling@gmail.com", 'f')}, 45, 100);
        Book culegereMate = new Book("Math problems",
                                      new Author[]{new Author("Mircea Dumitru", "mdumitru@gmail.com", 'm'), new Author("Ana Popa", "apopa@gmail.com", 'f')},
                                        30, 500);

        //testing toString method
        System.out.println(book1.toString());
        System.out.println(book2.toString());
        System.out.println(culegereMate.toString());
        System.out.println();
        // testing setters and getters
        System.out.println(book1.getName() + " by " + book1.getAuthors()[0] + ", " + book1.getPrice() + " lei (" + book1.getQtyInStock() + " left)");
        book1.setQtyInStock(50);
        book1.setPrice(72);
        System.out.println(book1.getName() + " by " + book1.getAuthors()[0] + ", " + book1.getPrice() + " lei (" + book1.getQtyInStock() + " left)");

        System.out.print(culegereMate.getName() + " by ");
        for(Author aut : culegereMate.getAuthors()){
            System.out.print(aut.toString() + ", ");
        }
        System.out.println(culegereMate.getPrice() + " lei (" + culegereMate.getQtyInStock() + " left)");

    }
}
