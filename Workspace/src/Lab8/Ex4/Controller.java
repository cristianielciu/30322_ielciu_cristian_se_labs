package Lab8.Ex4;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;

public class Controller {
    private static Controller controller;

    private TemperatureSensor temperatureSensor;
    private FireSensor[] fireSensor;
    private Unit alarmUnit;
    private Unit heatingUnit;
    private Unit coolingUnit;
    private Unit gsmUnit;

    private Controller(){
        temperatureSensor = new TemperatureSensor();
        
        fireSensor = new FireSensor[3];
        fireSensor[0] = new FireSensor(false);
        fireSensor[1] = new FireSensor(false);
        fireSensor[2] = new FireSensor(false);

        alarmUnit = new AlarmUnit();
        heatingUnit = new HeatingUnit();
        coolingUnit = new CoolingUnit();
        gsmUnit = new GsmUnit();
    }
    
    public static Controller getController(){
        if(controller == null){
            controller =  new Controller();
        }
        return controller;
    }

    public void setTemperature(int tmp){
        temperatureSensor.setValue(tmp);
    }
    public void setFire(int index, boolean isFire){
        fireSensor[index].setSmoke(isFire);
    }

    private boolean isFire(){
        for(int i = 0; i < fireSensor.length; i++){
            if(fireSensor[i].isSmoke()){
                return true;
            }
        }
        return false;
    } 

    private void putOffFire(){
        for(int i = 0; i < fireSensor.length; i++){
            fireSensor[i].setSmoke(false);
        }
    }

    public void takeAction(){
        try(PrintWriter pWriter = new PrintWriter(new FileWriter("system_logs.txt", true))){
            int tmp = temperatureSensor.getValue();
            if(isFire()){
                alarmUnit.startUnit();
                gsmUnit.startUnit();

                pWriter.write("Fire was detected in the building. Alarm and gsm units are started\n");
                putOffFire();
            }
            else if(tmp > 25){
                System.out.print(tmp + "C: ");
                coolingUnit.startUnit();

                pWriter.write("Temperature: " + tmp + "C. Cooling unit is started\n");
            }
            else if(tmp < 19){
                System.out.print(tmp + "C: ");
                heatingUnit.startUnit();

                pWriter.write("Temperature: " + tmp + "C. Heating unit is started\n");
            }

        }catch(IOException e){
            e.printStackTrace();
        }
    }

}

class TemperatureSensor {
    private int value;

     TemperatureSensor() {}
    
    public void setValue(int value) {
        this.value = value;
    }
    public int getValue() {
        return value;
    }
}

class FireSensor {
    private boolean isSmoke;

    public FireSensor(boolean isSmoke) {
        this.isSmoke = isSmoke;
    }
    
    boolean isSmoke(){
        return isSmoke;
    }
    public void setSmoke(boolean isSmoke) {
        this.isSmoke = isSmoke;
    }
}


interface Unit {
    public void startUnit();
}

class AlarmUnit implements Unit{
    public void startUnit(){
        System.out.println("Alarm is started");
    }
    
}

class CoolingUnit implements Unit{
    public void startUnit(){
        System.out.println("Cooling is started");
    }
}

class HeatingUnit implements Unit{
    public void startUnit(){
        System.out.println("Heating is started");
    }
}

class GsmUnit implements Unit{
    public void startUnit(){
        System.out.println("Owner called.");
    }
}