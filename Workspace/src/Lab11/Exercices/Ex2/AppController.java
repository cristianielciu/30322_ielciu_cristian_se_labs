package Lab11.Ex2;

import java.util.ArrayList;

public class AppController {
    private final AppModel model;
    private final AppView view;

    public ArrayList<AppModel> Products = new ArrayList<AppModel>();

    public AppController(AppModel model, AppView view) {
        this.model = model;
        this.view = view;
    }

    public void initController() {
        addButton();
        deleteButton();
        quantityButton();
        viewButton();
    }

    public void addButton() {
        view.addButton.addActionListener(e -> {
            AppModel product = new AppModel();
            product.setName(view.nameTF.getText());
            product.setPrice(Double.parseDouble(view.priceTF.getText()));
            product.setQuantity(Integer.parseInt(view.quantityTF.getText()));

            Products.add(product);
        });
    }

    public void deleteButton() {
        view.deleteButton.addActionListener(e -> {
            for (int i = 0; i < Products.size(); i++) {
                if (Products.get(i).getName().equals(view.nameTF.getText())) {
                    Products.remove(i);
                }
            }
        });
    }

    public void quantityButton() {
        view.quantityButton.addActionListener(e -> {
            for (int i = 0; i < Products.size(); i++) {
                if (Products.get(i).getName().equals(view.nameTF.getText())) {
                    Products.get(i).setQuantity(Integer.parseInt(view.quantityTF.getText()));
                }
            }
        });
    }

    public void viewButton() {
        view.viewButton.addActionListener(e -> {
            for (int i = 0; i < Products.size(); i++) {
                    model.setName(Products.get(i).getName());
                    model.setPrice(Products.get(i).getPrice());
                    model.setQuantity(Products.get(i).getQuantity());
                    view.textArea.append(model.toString() + "\n");
            }});
    }


}
