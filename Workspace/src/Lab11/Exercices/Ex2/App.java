package Lab11.Ex2;

public class App {
    public static void main(String[] args) {
        AppView view = new AppView();
        AppModel model = new AppModel();
        AppController controller = new AppController(model, view);
        controller.initController();
    }
}
