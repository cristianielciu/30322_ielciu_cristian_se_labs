package Lab11.Ex1;

import javax.swing.JFrame;
import javax.swing.JTextArea;

public class Sensor extends Thread {
    JFrame frame;
    JTextArea textArea;

    public Sensor(JFrame frame, JTextArea textArea) {
        this.frame = frame;
        this.textArea = textArea;
    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            textArea.append("Sensor: " + System.currentTimeMillis() + "\n");
            frame.repaint();
        }
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Sensor");
        frame.setSize(300, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JTextArea textArea = new JTextArea();
        frame.add(textArea);
        frame.setVisible(true);
        Sensor sensor = new Sensor(frame, textArea);
        sensor.start();
    }

}
