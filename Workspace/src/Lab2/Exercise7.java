package Lab2;

import java.util.Random;
import java.util.Scanner;

public class Exercise7 {

    public static void main(String[] args){
        Random random = new Random();
        Scanner read = new Scanner(System.in);

        int number = random.nextInt(20);
        int guess = -1;
        int no_of_attempts = 0;
        System.out.println(number);

        while(no_of_attempts < 3 && guess != number) {
            System.out.println("Guess the number!");
            guess = read.nextInt();
            if(guess < number){
                System.out.println("Wrong answer, your number it too low");
                no_of_attempts++;
            }
            else if(guess > number){
                System.out.println("Wrong answer, your number it too high");
                no_of_attempts++;
            }
            else System.out.println("You won");

        }
        if(no_of_attempts == 3)
            System.out.println("You lost");
    }


}
