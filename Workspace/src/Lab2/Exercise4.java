package Lab2;

public class Exercise4 {
    public static void main(String[] args){
        int[] a = {10, 20, 2, 14, 40, 22};
        int max = a[0];
        for(int i : a){
            if(i > max)  max = i;
        }
        System.out.println("max: " + max);
    }
}
