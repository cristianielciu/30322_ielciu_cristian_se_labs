package Lab2;

import java.util.Scanner;

public class Exercise3 {
    public static int counter = 0;

    public static void primeNumbers(int x) {
        boolean isPrime = true;
        for (int i = 2; i * i <= x; i++) {
            if (x % i == 0)
                isPrime = false;
        }
        if (isPrime) {
            counter++;
            System.out.print(x + " ");
        }
    }

    public static void main(String[] args) {
        int a, b;
        Scanner read = new Scanner(System.in);
        a = read.nextInt();
        b = read.nextInt();
        for(int i = a; i <= b; i++){
            primeNumbers(i);
        }
        System.out.println("\n" + counter + " prime numbers");
    }
}
