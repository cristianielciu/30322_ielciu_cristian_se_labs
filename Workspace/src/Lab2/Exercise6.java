package Lab2;

import java.util.Scanner;

public class Exercise6 {
    static int recursiveFactorial(int n){
        if(n == 1)  return 1;
        return n * recursiveFactorial(n-1);
    }
    static int factorial(int n){
        int f = 1;
        for (int i = 1; i <= n; i++){
            f *= i;
        }
        return f;
    }

    public static void main(String[] args){
        Scanner read = new Scanner(System.in);
        int n = read.nextInt();

        System.out.println(recursiveFactorial(n));
        System.out.println(factorial(n));
    }


}
