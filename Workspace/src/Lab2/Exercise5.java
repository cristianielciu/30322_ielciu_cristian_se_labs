package Lab2;

import java.util.Random;

public class Exercise5 {
    public static void bubbleSort(int[] a) {
        int n = a.length;
        for (int i = 0; i < n - 1; i++)
            for (int j = 0; j < n - i - 1; j++)
                if (a[j] > a[j + 1]) {
                    // swap arr[j+1] and arr[j]
                    int temp = a[j];
                    a[j] = a[j + 1];
                    a[j + 1] = temp;
                }
    }

    public static void main(String[] args) {
        Random rand = new Random();
        int[] a = new int[10];
        for (int i = 0; i < a.length; i++) {
            a[i] = rand.nextInt(100);
        }
        System.out.print("a: ");

        for (int i : a) {
            System.out.print(i + " ");
        }

        bubbleSort(a);
        System.out.print("\nNew a:");
        for (int i : a) {
            System.out.print(i + " ");
        }

    }
}

