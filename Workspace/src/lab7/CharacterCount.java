package lab7;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;


public class CharacterCount {
    public static void main(String[] args) {
        Scanner read = new Scanner(System.in);
        try (BufferedReader br = new BufferedReader(new FileReader("E:\\FACULTATE\\AN 2\\Sem2\\SE\\Bitbucket\\Workspace\\src\\lab7\\data.txt"))) {
            //char c = args[0].charAt(0);
            char c = read.nextLine().charAt(0);
            int count = 0;
            String line;
            while ((line = br.readLine()) != null) {
                for (int i = 0; i < line.length(); i++) {
                    if (c == line.charAt(i))
                        count++;
                }
            }
            System.out.println("The letter " + c + " appears " + count + " times in the file.");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
