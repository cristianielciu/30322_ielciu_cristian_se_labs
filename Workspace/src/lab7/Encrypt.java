package lab7;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class Encrypt {
    //WHEN I TRY TO RUN THE PROGRAM FROM COMMAND WINDOW I GET
    //Error: Could not find or load main class
    //So I tested it with system input
    public static void  main(String[] args){

//        String operation = args[0];
        String operation = "decrypt";

        //get the file name from the command line (wihtout the extension)
//        String file = args[1].substring(0, args[1].lastIndexOf('.'));
        String file = "E:\\FACULTATE\\AN 2\\Sem2\\SE\\Bitbucket\\Workspace\\src\\lab7\\text.enc";

        String line;
        String text = "";
        
        if(operation.equals("encrypt")){
            
            try(BufferedReader br = new BufferedReader(new FileReader(file))){
                while((line = br.readLine()) != null){
                    text += encrypt(line) + '\n';
                }
            }catch(IOException e){
                e.printStackTrace();
            }
            file = file.substring(0, file.lastIndexOf('.'));
            writeToFile(file + ".enc", text);
        }
        else if(operation.equals("decrypt")){
            
            try(BufferedReader br = new BufferedReader(new FileReader(file))){
                    while((line = br.readLine()) != null){
                        text += decrypt(line) + '\n';
                    }
            }catch(IOException e){
                e.printStackTrace();
            }

            file = file.substring(0, file.lastIndexOf('.'));
            writeToFile(file + ".dec", text);
        }
        else{
            System.out.println("Invalid operation");
        }
    }

    static String encrypt(String line) {
        char[] encryptedLine = null;
        int asciiValue;

        encryptedLine = line.toCharArray();
        for(int i = 0; i < encryptedLine.length; i++){
            asciiValue = (int) encryptedLine[i];
            asciiValue--;
            encryptedLine[i] = (char) asciiValue;
        }
        line = String.valueOf(encryptedLine);
        return line;
    }
    
    static String decrypt(String line) {
        char[] decryptedLine = null;
        int asciiValue;

        decryptedLine = line.toCharArray();
        for(int i = 0; i < decryptedLine.length; i++){
            asciiValue = (int) decryptedLine[i];
            asciiValue++;
            decryptedLine[i] = (char) asciiValue;
        }
        line = String.valueOf(decryptedLine);
        return line;
    }

    
    static void writeToFile(String filename, String text){
        
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(filename))){
            bw.write(text);
        }catch(IOException e){
            e.printStackTrace();
        }
    }
      
}
