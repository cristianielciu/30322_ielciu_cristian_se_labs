package lab7;

public class CoffeTest {
    public static void main(String[] args) {
        CofeeMaker mk = new CofeeMaker();
        CofeeDrinker d = new CofeeDrinker();

        for(int i = 0;i<15;i++){
            try{
                Cofee c = mk.makeCofee();
                try {
                    d.drinkCofee(c);
                } catch (TemperatureException e) {
                    System.out.println("Exception:"+e.getMessage()+" temp="+e.getTemp());
                } catch (ConcentrationException e) {
                    System.out.println("Exception:"+e.getMessage()+" conc="+e.getConc());
                }
                finally{
                    System.out.println("Throw the cofee cup.\n");
                }
            }catch (NoOfCoffeeException e){
                System.out.println("Exception:"+ e.getMessage() + "You had enough coffee.");
            }
        }
    }
}//.class


class CofeeMaker {
    Cofee makeCofee() throws NoOfCoffeeException{
        if(Cofee.getNoOfCoffees() >= 10){
            throw new NoOfCoffeeException(Cofee.getNoOfCoffees(), "There is no coffee left. ");
        }
        System.out.println("Make a coffe");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        Cofee cofee = new Cofee(t,c);
        return cofee;
    }

}//.class

class Cofee{
    private static int noOfCoffees = 0;
    private int temp;
    private int conc;


    Cofee(int t,int c){ noOfCoffees++;temp = t;conc = c;}
    int getTemp(){return temp;}
    int getConc(){return conc;}
    static int getNoOfCoffees(){return  noOfCoffees;}

    public String toString(){return "[cofee temperature="+temp+":concentration="+conc+"]";}
}//.class

class CofeeDrinker{
    void drinkCofee(Cofee c) throws TemperatureException, ConcentrationException{
        if(c.getTemp()>60)
            throw new TemperatureException(c.getTemp(),"Cofee is to hot!");
        if(c.getConc()>50)
            throw new ConcentrationException(c.getConc(),"Cofee concentration to high!");
        System.out.println("Drink cofee:"+c);
    }
}//.class

class TemperatureException extends Exception{
    int t;
    public TemperatureException(int t,String msg) {
        super(msg);
        this.t = t;
    }

    int getTemp(){
        return t;
    }
}//.class

class ConcentrationException extends Exception{
    int c;
    public ConcentrationException(int c,String msg) {
        super(msg);
        this.c = c;
    }

    int getConc(){
        return c;
    }
}//.class

class NoOfCoffeeException extends Exception{
    int cNo;
    public NoOfCoffeeException(int cNo, String msg){
        super(msg);
        this.cNo = cNo;
    }

    public int getcNo() {
        return cNo;
    }
}