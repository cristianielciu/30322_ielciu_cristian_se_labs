package lab7;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;

public class Car implements Serializable{
    private String model;
    private double price;

    public Car(){}

    public Car(String model, double price) {
        this.model = model;
        this.price = price;
    }

    public String getModel(){
        return model;
    }


    public void saveCar(String filename){
        try(ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(filename))){
            out.writeObject(this);
            System.out.println("Car saved");
        } catch (IOException e) {
            System.out.println("Error saving car");
        }
    }

    public static Car loadCar(String filename){
        Car car = new Car();
        try(ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename))){
            car = (Car) in.readObject();
            System.out.println("Car loaded");
        }catch(IOException e){
            System.out.println("Could not open the file");; e.printStackTrace();
        }catch(ClassCastException e){
            System.out.println("Class not found");
        }finally{
            return car;
        }
    }

    public static void readCar(String filename){
        try(ObjectInputStream in = new ObjectInputStream(new FileInputStream(filename))){
            Car car = (Car) in.readObject();
            System.out.println(car);
        }catch(IOException e){
            System.out.println("Could not open the file");;
        }catch(ClassNotFoundException e){
            System.out.println("Class not found");
        }
    }

    @Override
    public String toString() {
        return "Car{" +
                "model = '" + model + '\'' +
                ", price = " + price +
                '}';
    }



    public static void main(String[] args){
        ArrayList<Car> cars = new ArrayList<>();
        cars.add(new Car("BMW", 100000));
        cars.add(new Car("Audi", 90000));
        cars.add(new Car("Citroen", 40000));

        for(Car car : cars){
            car.saveCar(car.getModel() + ".car");
        }
        
        Car car = Car.loadCar("BMW.car");
        System.out.println(car);

        Car.readCar("Audi.car");

    }



    
}
