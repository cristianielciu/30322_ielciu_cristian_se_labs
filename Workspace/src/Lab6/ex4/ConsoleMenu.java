package Lab6.ex4;

import javax.swing.text.html.HTMLDocument;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;
import java.util.Set;

public class ConsoleMenu {
    public static void main(String[] args){
        Dictionary dictionary = new Dictionary();

        Scanner read = new Scanner(System.in);

        char response;
        String word, def;

        do {
            System.out.println("Menu");
            System.out.println("a - Add word");
            System.out.println("s - Search word");
            System.out.println("w - List all words");
            System.out.println("d - List all definitions");
            System.out.println("l - List dictionary");
            System.out.println("e - Exit");

            word = read.nextLine();
            response = word.charAt(0);

            switch (response){
                case 'a' : case 'A':
                    System.out.println("Add the word: ");
                    word = read.nextLine();
                    if(word.length() > 1){
                        System.out.println("Add the definition: ");
                        def = read.nextLine();
                        dictionary.addWord(new Word(word), new Definition(def));
                    }
                    break;

                case 's' : case 'S':
                    System.out.println("Searched word: ");
                    word = read.nextLine();

                    if(dictionary.getDefinition(word) != null){
                        System.out.println(word + ": " + dictionary.getDefinition(word));
                    }
                    else System.out.println(word + " IS NOT FOUND IN THE DICTIONARY");
                    break;

                case 'w' : case 'W':
                    Set<Word> words = dictionary.getAllWords();
                    Iterator i = words.iterator();
                    while(i.hasNext()){
                        System.out.println(i.next());
                    }
                    break;

                case 'd' : case 'D':
                    List<String> definitions = dictionary.getAllDefinitions();

                    for(String d : definitions){
                        System.out.println(d);
                    }
                    break;

                case 'l' : case 'L':
                    dictionary.listDictionary();
                    break;

            }
        }while(response != 'e' && response != 'E');

    }
}
