package Lab6.ex4;

public class Word {
    private String name;

    public Word(String name){
        this.name = name;
    }

    @Override
    public String toString(){
        return name;
    }

}
