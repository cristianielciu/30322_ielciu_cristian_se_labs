package Lab6.ex1;

import java.util.Comparator;

// I implemented 2 methods for sorting: with Comparable and with Comparator, just to see de differences
public class BankAccount implements Comparable
{
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance){
        this.owner = owner;
        this.balance = balance;
    }
    public void withdraw(double amount){
        balance = balance - amount;
        System.out.println("New balance: " + balance + " RON");
    }

    @Override
    public boolean equals(Object o){
        //Check if o is an instance of BankAccount or not
        if( o == null || !(o instanceof BankAccount)) {
            return false;
        }

        //Typecast o to BankAccount so that we can compare data members
        BankAccount bankAccount = (BankAccount) o;

        return (balance == bankAccount.balance && owner.equals(bankAccount.owner));

    }

    @Override
    public int hashCode(){
        return (int) (balance + owner.hashCode());
    }

    @Override
    public String toString(){
        return owner + ": " + balance + " RON";
    }


    // For Exercise2
    public void deposit(double amount){
        balance += amount;
    }

    public double getBalance() {
        return balance;
    }
    public String getOwner(){
        return owner;
    }

    @Override
    public int compareTo(Object o) {
        BankAccount bankAccount = (BankAccount) o;
        return owner.compareTo(bankAccount.owner);
    }

    public static Comparator<BankAccount> BankAccountOwner = new Comparator<BankAccount>() {
        @Override
        public int compare(BankAccount o1, BankAccount o2) {
            String owner1 = o1.getOwner().toUpperCase();
            String owner2 = o2.getOwner().toUpperCase();

            return owner1.compareTo(owner2);
        }
    };

    public static Comparator<BankAccount> BankAccountBalance = new Comparator<BankAccount>() {
        @Override
        public int compare(BankAccount o1, BankAccount o2) {
            double balance1 = o1.getBalance();
            double balance2 = o2.getBalance();

            return (int)(balance1 - balance2);
        }
    };
}
