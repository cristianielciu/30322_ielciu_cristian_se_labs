package Lab6.ex1and2;

import java.util.ArrayList;
import java.util.Collections;


// The implementation using Comparable interface is commented
public class Bank{
    private ArrayList<BankAccount> bankAccounts = new ArrayList<BankAccount>();

    public void addAccount(String owner, double balance) {
        bankAccounts.add(new BankAccount(owner, balance));
    }

    public void printAccounts(){
        //sortByBalance();
        Collections.sort(bankAccounts, BankAccount.BankAccountBalance);

        System.out.println("\nBank accounts sorted by balance:");
        for (BankAccount bankAccount : bankAccounts) {
            System.out.println(bankAccount);
        }
    }

    public void printAccounts(double minBalance, double maxBalance){
        System.out.println("\nBank accounts with the balance in the range " + minBalance +  " - " + maxBalance + " RON:");
        for (BankAccount bankAccount : bankAccounts) {
            if(bankAccount.getBalance() >= minBalance && bankAccount.getBalance() <= maxBalance){
                System.out.println(bankAccount);
            }
        }
    }

    public BankAccount getAccount(String owner){
        for (BankAccount bankAccount: bankAccounts) {
            if(bankAccount.getOwner().equals(owner))
                return bankAccount;
        }
        return null;
    }

    public ArrayList<BankAccount> getAllAccounts(){
        //Collections.sort(bankAccounts);

        Collections.sort(bankAccounts, BankAccount.BankAccountOwner);

        return bankAccounts;
    }


    private void sortByBalance(){
        boolean sorted = false;
        int unsortedLength = bankAccounts.size() - 1;

        while(!sorted) {
            sorted = true;
            for(int j = 0; j < unsortedLength; j++) {

                if (bankAccounts.get(j).getBalance() > bankAccounts.get(j + 1).getBalance()) {
                    BankAccount tmp;
                    tmp = bankAccounts.get(j);
                    bankAccounts.set(j, bankAccounts.get(j+1));
                    bankAccounts.set(j+1, tmp);
                    sorted = false;
                }
            }
        }
    }

}
