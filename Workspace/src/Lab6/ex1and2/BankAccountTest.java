package Lab6.ex1and2;

public class BankAccountTest {
    public static void main(String[] args){
        BankAccount bankAccount1 = new BankAccount("Ielciu Cristian", 1250 );
        BankAccount bankAccount2 = new BankAccount("Ielciu Cristian", 1250 );
        BankAccount bankAccount3 = new BankAccount("Popa Alexandru", 1250 );

        System.out.println(bankAccount1.equals(bankAccount2));
        System.out.println(bankAccount1.equals(bankAccount3));

        System.out.println("bankAccount1 HashCode: " + bankAccount1.hashCode());
        System.out.println("bankAccount2 HashCode: " + bankAccount2.hashCode());
        System.out.println("bankAccount3 HashCode: " + bankAccount3.hashCode());
    }


}
