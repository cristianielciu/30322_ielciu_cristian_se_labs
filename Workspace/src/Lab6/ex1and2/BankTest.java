package Lab6.ex1and2;

import java.util.ArrayList;

public class BankTest {
    public static void main(String[] args){
        Bank bancaTransilvania = new Bank();
        ArrayList<BankAccount> bankAccounts;



        bancaTransilvania.addAccount("Popa Alexandru", 3250);
        bancaTransilvania.addAccount("Ielciu Cristian", 2300);
        bancaTransilvania.addAccount("Radoi Vlad", 1000);
        bancaTransilvania.addAccount("Miron Mircea", 5700);
        bancaTransilvania.addAccount("Soporan Marian", 1500);
        bancaTransilvania.addAccount("Iurian Alin", 2400);

        bancaTransilvania.printAccounts(2000,4000);
        bancaTransilvania.printAccounts();

        System.out.println("\n" + bancaTransilvania.getAccount("Ielciu Cristian"));


        bankAccounts = bancaTransilvania.getAllAccounts();
        System.out.println("\nBank accounts sorted by owner's name: ");
        for(BankAccount bA : bankAccounts){
            System.out.println(bA);
        }
    }
}
