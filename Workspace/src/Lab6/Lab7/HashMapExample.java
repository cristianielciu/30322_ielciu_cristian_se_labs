package Lab7;

import java.util.HashMap;
import java.util.Iterator;

public class HashMapExample{

    public static void main(String args[]){

        HashMap hashMap = new HashMap();
//------
        hashMap.put( "One", new Integer(1) ); // adding value into HashMap
        hashMap.put( "Two", new Integer(2) );
        hashMap.put( "Three", new Integer(3) );
//------
        System.out.println("HashMap contains " + hashMap.size() + " key value pair.");
//------
        if( hashMap.containsValue( new Integer(1) ) ){
            System.out.println("HashMap contains 1 as value");
        }else{
            System.out.println("HashMap does not contain 1 as value");
        }
//-------
        if( hashMap.containsKey("One") ){
            System.out.println("HashMap contains One as key");
        }else{
            System.out.println("HashMap does not contain One as value");
        }
//-------
        Integer one = (Integer) hashMap.get("One");
        System.out.println("Value mapped with key \"One\" is " + one);
//-------
        System.out.println("Retriving all keys from the HashMap");
        Iterator iterator = hashMap.keySet().iterator();
        while( iterator. hasNext() ){
            System.out.println( iterator.next() );
        }
//-------
        System.out.println("Retriving all values from the HashMap");
        iterator = hashMap.entrySet().iterator();
        while( iterator. hasNext() ){
            System.out.println( iterator.next() );
        }
//-------
        System.out.println( hashMap.remove("One") + " is removed from the HashMap.");
    }
}