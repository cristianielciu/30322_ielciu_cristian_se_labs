package Lab7;

import java.lang.reflect.Method;

public class Point {

    private int x;
    private int y;

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }

    public String toString(){
        return "Point["+x+","+y+"]";
    }

    public static void main(String[] args) { Point p = new Point();

        Class co = p.getClass();

        System.out.println("Class name for object "+p+" is +"+co.getSimpleName());

        System.out.println("List of methods for class "+co.getSimpleName()+":");

        Method[] ms = co.getDeclaredMethods();

        for(Method m:ms)
            System.out.println(m.getName());

    }

}

