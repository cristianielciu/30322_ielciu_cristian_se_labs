package Lab7;

import java.util.TreeSet;
public class TestSort {
    public static void main(String[] args) {
        TreeSet t = new TreeSet();
        Person1 p1 = new Person1("jon",4);
        Person1 p2 = new Person1("alin",10);
        Person1 p3 = new Person1("dan",8);
        Person1 p4 = new Person1("florin",7);
        t.add(p1);t.add(p2);t.add(p3);t.add(p4);
        System.out.println(t);
        System.out.println("firs:"+t.first());
        System.out.println("last:"+t.last());
        System.out.println("subset:"+t.subSet(new Person1("x",5),new Person1("y",9)));
        System.out.println("headset:"+t.headSet(p3));
    }
}

class Person1 implements Comparable{
    int age;
    String name;
    Person1(String n,int a){
        age = a;
        name = n;
    }

    public int compareTo(Object o) {
        Person1 p = (Person1)o;
        if(age>p.age) return 1;
        if(age==p.age) return 0;
        return -1;
    }

    public String toString(){
        return "("+name+":"+age+")";
    }
}
