package Lab7;

import java.util.*;
public class TestSet1 {

    static void displayAll(Set list){
        System.out.println("- - - - - - - -");
        Iterator i = list.iterator();
        while(i.hasNext()){
            String s = (String)i.next();
            System.out.println(s);
        }
    }

    /**
     * @param args
     */
    public static void main(String[] args) {

        HashSet set = new HashSet();
        set.add("one");set.add("two");set.add("six");set.add("six");
        set.add("one");set.add("four");set.add("five");
        displayAll(set);

        TreeSet tree = new TreeSet();
        tree.add("one");tree.add("two");tree.add("six");tree.add("six");
        tree.add("one");tree.add("four");tree.add("five");
        displayAll(tree);

        LinkedHashSet lnk = new LinkedHashSet();
        lnk.add("one");lnk.add("two");lnk.add("six");lnk.add("six");
        lnk.add("one");lnk.add("four");lnk.add("five");
        displayAll(lnk);
    }
}
