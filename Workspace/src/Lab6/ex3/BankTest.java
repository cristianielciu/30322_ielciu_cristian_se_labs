package Lab6.ex3;

import Lab6.ex1and2.BankAccount;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.TreeSet;

public class BankTest {
    public static void main(String[] args){
        Bank bancaTransilvania = new Bank();
        TreeSet<BankAccount> bankAccounts;


        bancaTransilvania.addAccount("Popa Alexandru", 3250);
        bancaTransilvania.addAccount("Ielciu Cristian", 2300);
        bancaTransilvania.addAccount("Radoi Vlad", 1000);
        bancaTransilvania.addAccount("Miron Mircea", 5700);
        bancaTransilvania.addAccount("Soporan Marian", 1500);
        bancaTransilvania.addAccount("Iurian Alin", 2400);


        bancaTransilvania.printAccounts();

        bancaTransilvania.printAccounts(1500,4000);

        System.out.println("\ngetAccount(owner) method: ");
        System.out.println(bancaTransilvania.getAccount("Ielciu Cristian"));


        bankAccounts = bancaTransilvania.getAllAccounts();
        System.out.println("\nBank accounts sorted by owner's name: ");

        Iterator i = bankAccounts.iterator();
        while(i.hasNext()){
            BankAccount bA = (BankAccount) i.next();
            System.out.println(bA);
        }

    }
}