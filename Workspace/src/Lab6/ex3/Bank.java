package Lab6.ex3;

import Lab6.ex1and2.BankAccount;

import java.util.*;

public class Bank {
    TreeSet<BankAccount> bankAccounts = new TreeSet<BankAccount>();

    public void addAccount(String owner, double balance) {
        bankAccounts.add(new BankAccount(owner, balance));
    }


    public void printAccounts(){
        // declare a local tree which sorts accounts by balance
        TreeSet<BankAccount> tmp = new TreeSet<BankAccount>(new Comparator<BankAccount>() {
            @Override
            public int compare(BankAccount o1, BankAccount o2) {
                return (int)(o1.getBalance()- o2.getBalance());
            }
        });

        tmp.addAll(bankAccounts);

        System.out.println("Bank accounts sorted by balance: ");

        Iterator i = tmp.iterator();
        while(i.hasNext()){
            BankAccount bA = (BankAccount) i.next();
            System.out.println(bA);
        }
    }


    public void printAccounts(double minBalance, double maxBalance){
        System.out.println("\nBank accounts with the balance in the range " + minBalance +  " - " + maxBalance + " RON:");

        Iterator i = bankAccounts.iterator();

        while(i.hasNext()){
            BankAccount bA = (BankAccount) i.next();
            if(bA.getBalance() >= minBalance && bA.getBalance() <= maxBalance) {
                System.out.println(bA);
            }
        }
    }

    public BankAccount getAccount(String owner) {

        Iterator i = bankAccounts.iterator();

        while (i.hasNext()) {
            BankAccount bA = (BankAccount) i.next();
            if (bA.getOwner().equals(owner)) {
                return bA;
            }
        }
        return null;
    }

    public TreeSet<BankAccount> getAllAccounts(){
        return bankAccounts;
    }

}
