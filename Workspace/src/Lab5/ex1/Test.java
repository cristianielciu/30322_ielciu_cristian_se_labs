package Lab5.ex1;

import Lab4.ex6.Circle;
import Lab4.ex6.Rectangle;
import Lab4.ex6.Shape;
import Lab4.ex6.Square;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args){
        ArrayList<Shape>  shapes = new ArrayList<>();
        shapes.add(new Lab4.ex6.Rectangle(2,3, "red",true));
        shapes.add(new Lab4.ex6.Square(4, "blue", false));
        shapes.add(new Lab4.ex6.Circle(1,"green", true));


        for(Shape shape : shapes) {
            System.out.println(shape.toString());
        }

        System.out.println("\nThe rectangle has the width " + ((Rectangle)shapes.get(0)).getWidth() +
                ", the legth " + ((Rectangle)shapes.get(0)).getLength() +
                ", the area " + ((Rectangle)shapes.get(0)).getArea() +
                " and the perimeter " + ((Rectangle)shapes.get(0)).getPerimeter());


        ((Square)shapes.get(1)).setLength(10);
        System.out.println("\nThe square has the side " +
                ((Square)shapes.get(1)).getWidth() +
                ", the area " + ((Square)shapes.get(1)).getArea() +
                " and the perimeter " + ((Square)shapes.get(1)).getPerimeter());

        System.out.println("\nThe circle has the radius " + ((Circle)shapes.get(2)).getRadius() +
                ", the area " + ((Circle) shapes.get(2)).getArea() +
                " and the perimeter " + ((Circle) shapes.get(2)).getPerimeter());
    }
}