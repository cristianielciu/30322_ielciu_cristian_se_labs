package Lab5.ex1;


public class Square extends Rectangle {
    public Square(){}
    public Square(double side){
        super.setWidth(side);
    }
    public Square(double side, String color, boolean filled){
        super.setWidth(side);
        super.setColor(color);
        super.setFilled(filled);
    }
    public double getSide(){
        return getWidth();
    }
    public void setSide(double side){
        setWidth(side);
    }

    @Override
    public void setLength(double side){
        super.setWidth(side);
    }

    @Override
    public void setWidth(double side){
        super.setWidth(side);
    }
    @Override
    public double getArea(){ return Math.pow(getWidth(),2);}

    @Override
    public double getPerimeter(){ return 4*getWidth();}

    @Override
    public String toString(){
        return "A Square with side = " + getSide() + ", which is a subclass of " + super.toString();
    }
}