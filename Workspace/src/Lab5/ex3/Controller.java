package Lab5.ex3;


public class Controller {
    private static Controller controller;

    private TemperatureSensor temperatureSensor = new TemperatureSensor();
    private LightSensor lightSensor = new LightSensor();

    private Controller(){}

    public static Controller getController(){
        if(controller == null)
            controller = new Controller();
        return controller;
    }

    public void  control(){
        for(int i = 1; i <= 20; i++) {
            System.out.println("Temperature: " + temperatureSensor.readValue());
            System.out.println("Light: " + lightSensor.readValue());
            wait(1000);
        }
    }

    public static void wait(int ms){
        try {
            Thread.sleep(ms);
        }
        catch (InterruptedException ex){
            Thread.currentThread().interrupt();
        }
    }
}
