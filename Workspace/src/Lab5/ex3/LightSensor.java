package Lab5.ex3;

import java.util.Random;

public class LightSensor extends Sensor{
    @Override
    public int readValue(){
        Random random = new Random();
        return random.nextInt(100);
    }
}
