package Lab5.ex3;

public class Test {
    public static void main(String[] args){
       //   can't use constructor after modifying the controller class
        // Controller controller = new Controller();
       // controller.control();

        Controller firstController = Controller.getController();
        firstController.control();

        //Has the same address as firstController (these 2 are the same)
        // Controller secondController = Controller.getController();
        //secondController.control();
    }
}
