package Lab5.ex2;

public class ProxyImage implements Image{
//create RotatedImage attribute and a boolean rotate, pass the rotate in the constructor of the ProxyImage, 
//then in the display, check for the rotate, if it is true then call the display of the RotatedImage, else call the one of the RealImage
    private Image realImage;
    private String fileName;
    private boolean rotate;

    public ProxyImage(Image image, boolean rotate){
        System.out.println("\nFrom ProxyImage constructor: ");
        this.rotate = rotate;
        if(rotate == true){
            realImage = (RotatedImage) image;
        }
        else{
            realImage = (RealImage) image;
        }
        realImage.display();
    }
    public ProxyImage(String fileName){
        this.fileName = fileName;
    }

    @Override
    public void display() {
        if(realImage == null){
            realImage = new RealImage(fileName);
        }
        realImage.display();
    }
}