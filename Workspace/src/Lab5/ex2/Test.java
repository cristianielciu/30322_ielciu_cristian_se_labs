package Lab5.ex2;

import java.util.ArrayList;

public class Test {
    public static void main(String[] args){
        ArrayList<Image> images = new ArrayList<Image>();
        images.add(new RealImage("C:\\Users\\cristian\\Pictures"));
        images.add(new ProxyImage("C:\\Users\\andreea\\Pictures\\New Pictures"));
        images.add(new RotatedImage("C:\\Users\\ana\\Pictures\\2021"));

        images.get(0).display();
        images.get(1).display();
        images.get(2).display();

        images.add(new ProxyImage(images.get(0), false));
        images.add(new ProxyImage(images.get(2), true));

    }

}
