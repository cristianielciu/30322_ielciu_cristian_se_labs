//Create 2 counters implemented using threads. First counter is incremented from 0 to 100. Second counter is incremented from 100 to 200 and start only after the first counter finish it's job.
public class Counter extends Thread{
    static int cnt = 0;
    Thread t;
    private String n;

    public Counter(String n, Thread t){
        this.n = n;
        this.t = t;
    }

    public void run(){
        System.out.println("Process " + n + " is running.");
        try{
            if(t != null) t.join();
            System.out.println("Process " + n + " is counting: ");
            for(int i = 0; i < 100; i++){
                System.out.println(++cnt);
            }
        }
        catch(InterruptedException e){
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Counter c1 = new Counter("C1", null);
        Counter c2 = new Counter("C2", c1);
        c1.start();
        c2.start();
    }
}
