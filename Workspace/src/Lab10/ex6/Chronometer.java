package Lab10.ex6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Chronometer extends Thread implements ActionListener {
    JFrame frame;
    JButton b1, b2;
    JTextField textField;
    boolean state = false;
    int count = 0;

    private final int HEIGHT = 30;
    private final int WIDTH = 100;

    public Chronometer() {
        frame = new JFrame("Chronometer");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(450, 100);
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setLayout(null);


        textField = new JTextField();
        textField.setBounds(260,20, WIDTH, HEIGHT);
        frame.add(textField);

        b1 = new JButton("Start");
        b1.setBounds(20,20, WIDTH, HEIGHT);
        b1.addActionListener(this);
        frame.add(b1);

        b2 = new JButton("Reset");
        b2.setBounds(140,20, WIDTH, HEIGHT);;
        b2.addActionListener(this);
        frame.add(b2);



        frame.setVisible(true);

    }

    public static void main(String[] args) {
        Chronometer c1 = new Chronometer();
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == b1)
            if (!state) {
                b1.setText("Stop");
                state = true;

                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        while (state) {
                            try {
                                Thread.sleep(1000);
                                textField.setText(String.valueOf(count));
                                count++;
                            } catch (InterruptedException exc) {
                                System.out.println(exc.getMessage());
                            }
                        }
                    }
                }).start();
            } else {
                b1.setText("Start");
                state = false;
            }else if(e.getSource() == b2){
            count = 0;
            state = false;
        }
    }

}